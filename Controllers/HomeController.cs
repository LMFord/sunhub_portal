﻿using System.Web.Mvc;
using SunshareQAQC.Models;

namespace SunshareQAQC.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeViewModels _IndexViewModels;

        public HomeController()
        {
            //_IndexViewModels = new HomeViewModels();
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult SignedContracts()
        {
            //ModelState.Clear();
            //var model = _IndexViewModels;
            //return View("SignedContracts", model);
            return View();
        }

        public ActionResult CreditApprovalCustomers()
        {
            return View();
        }

        public ActionResult CreditDeclinedCustomers()
        {
            return View();
        }

        public ActionResult RequestUtilityUsage()
        {
            return View();
        }

        public ActionResult ReceivedUtilityUsage()
        {
            return View();
        }
        public ActionResult GardenAllocatedCustomer()
        {
            return View();
        }
        public ActionResult ProductionReadyCustomer()
        {
            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}