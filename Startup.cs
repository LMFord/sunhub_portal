﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SunshareQAQC.Startup))]
namespace SunshareQAQC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
