﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace SunshareQAQC.Extensions
{
    public static class IdentityExtension
    {
        public static string GetDisplayName(this IIdentity identity, string UserID)
        {
            string Name="";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select dbo.CGA_udf_GetDisplayName("+ UserID + ")";
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        Name = sdr[0].ToString();
                    }
                }
                con.Close();
                con.Dispose();
            }
            return Name;
        }
        public static string GetPartnerImageUrl(this IIdentity identity, string UserID)
        {
            string DP = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select dbo.CGA_udf_GetDisplayPicture(" + UserID + ")";
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        DP = sdr[0].ToString();
                    }
                }
                con.Close();
                con.Dispose();
            }
            return DP;
        }
    }
}