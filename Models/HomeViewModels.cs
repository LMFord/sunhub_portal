﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SunshareQAQC.Models
{
    public class HomeViewModels
    {
        public ICollection<SignedContracts> LstSignedContracts { get; set; }

        public HomeViewModels()
        {
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        //    {
        //        SignedContracts objSignedContracts;
        //        LstSignedContracts = new List<SignedContracts>();
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.Connection = con;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "CGA_usp_Get_Signed_Contracts";
        //        con.Open();
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        if(sdr.HasRows)
        //        {
        //            while(sdr.Read())
        //            {
        //                objSignedContracts = new SignedContracts();
        //                objSignedContracts.LeadId = Convert.ToInt32(sdr["LeadId"]);
        //                objSignedContracts.SalesRepID = Convert.ToInt32(sdr["AspNetUsersId"]);
        //                objSignedContracts.FirstName = sdr["FirstName"].ToString();
        //                objSignedContracts.LastName = sdr["LastName"].ToString();
        //                objSignedContracts.Phone = sdr["MobileNo"].ToString();
        //                objSignedContracts.Email = sdr["PrimaryEmail"].ToString();
        //                objSignedContracts.State = sdr["StateName"].ToString();
        //                objSignedContracts.Country = sdr["CountyName"].ToString();
        //                LstSignedContracts.Add(objSignedContracts);
        //            }
        //        }
        //        con.Close();
        //        con.Dispose();
        //    }
        }
    }
    public class SignedContracts
    {
        public int LeadId { get; set; }
        public int SalesRepID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

    }
}