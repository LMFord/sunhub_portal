﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using SunshareQAQC.Controllers;
using Component = Castle.MicroKernel.Registration.Component;

namespace SunshareQAQC
{
    public class Installer : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(FindControllers().Configure(ConfigureControllers()));
            //Install Models
            
        }

        private BasedOnDescriptor FindControllers()
        {
            return AllTypes.From()
                .BasedOn<IController>()
                .If(Component.IsInSameNamespaceAs<HomeController>())
                .If(t => t.Name.EndsWith("Controller"));
        }
        private ConfigureDelegate ConfigureControllers()
        {
            return c => c.LifeStyle.Transient;
        }
    }
}
