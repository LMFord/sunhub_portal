﻿using System.Web;
using System.Web.Optimization;

namespace SunshareQAQC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
               "~/Scripts/jquery/dist/jquery.min.js",
               "~/Scripts/bootstrap/dist/js/bootstrap.min.js",
               "~/Scripts/fastclick/lib/fastclick.js",
               "~/Scripts/nprogress/nprogress.js",
                "~/Scripts/iCheck/icheck.min.js",
                 "~/Scripts/datatables.net/js/jquery.dataTables.min.js",
                 "~/Scripts/datatables.net-bs/js/dataTables.bootstrap.min.js",
                 "~/Scripts/datatables.net-buttons/js/dataTables.buttons.min.js",
                 "~/Scripts/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
                 "~/Scripts/datatables.net-buttons/js/buttons.flash.min.js",
                 "~/Scripts/datatables.net-buttons/js/buttons.html5.min.js",
                 "~/Scripts/datatables.net-buttons/js/buttons.print.min.js",
                 "~/Scripts/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
                 "~/Scripts/datatables.net-keytable/js/dataTables.keyTable.min.js",
                 "~/Scripts/datatables.net-responsive/js/dataTables.responsive.min.js",
                 "~/Scripts/datatables.net-responsive-bs/js/responsive.bootstrap.js",
                 "~/Scripts/datatables.net-scroller/js/datatables.scroller.min.js",
                 "~/Scripts/jszip/dist/jszip.min.js",
                 "~/Scripts/pdfmake/build/pdfmake.min.js",
                 "~/Scripts/pdfmake/build/vfs_fonts.js",
                 "~/Scripts/clock/date.js",
                 "~/Scripts/clock/jquery.clock.js", 
                 "~/Scripts/build/js/custom.min.js"
                ));

            bundles.Add(new StyleBundle("~/Scripts/bundle/css").Include(
            "~/Scripts/bootstrap/dist/css/bootstrap.min.css",
            "~/Scripts/font-awesome/css/font-awesome.min.css",
            "~/Scripts/nprogress/nprogress.css",
            "~/Scripts/iCheck/skins/flat/green.css",
            "~/Scripts/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css",
            "~/Scripts/jqvmap/dist/jqvmap.min.css",
            "~/Scripts/build/css/animate.min.css",
            "~/Scripts/animate.css/animate.css",
            "~/Scripts/datatables.net-bs/css/dataTables.bootstrap.min.css",
            "~/Scripts/datatables.net-buttons-bs/css/buttons.bootstrap.min.css",
            "~/Scripts/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css",
            "~/Scripts/datatables.net-responsive-bs/css/responsive.bootstrap.min.csss",
            "~/Scripts/datatables.net-scroller-bs/css/scroller.bootstrap.min.css",
             "~/Scripts/clock/analog.css",
             "~/Scripts/clock/digital.css",
            "~/Scripts/build/css/custom.min.css"
                ));
            bundles.Add(new StyleBundle("~/Scripts/bundle/login").Include(
             "~/Scripts/bootstrap/dist/css/bootstrap.min.css",
             "~/Scripts/build/css/base.css",
             "~/Scripts/build/css/responsive.css",
              "~/Scripts/build/css/RobotSlab.css"
            ));
            bundles.Add(new ScriptBundle("~/bundles/login").Include(
              "~/Scripts/jquery/dist/jquery.min.js",
              "~/Scripts/bootstrap/dist/js/bootstrap.min.js"
               ));

        }
    }
}
